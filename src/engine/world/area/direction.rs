#[derive(Copy, Clone, PartialEq)]
pub enum Direction
{
    North,
    Northwest,
    West,
    Southwest,
    South,
    Southeast,
    East,
    Northeast,
    Up,
    Down
}

impl Direction
{
    pub fn get_dir(alias: &str) -> Result<Direction, &'static str>
    {
        match alias
        {
            "n" | "north"           => Ok(Direction::North),
            "nw" | "northwest"      => Ok(Direction::Northwest),
            "w" | "west"            => Ok(Direction::West),
            "sw" | "southwest"      => Ok(Direction::Southwest), 
            "s" | "south"           => Ok(Direction::South),
            "se" | "southeast"      => Ok(Direction::Southeast),
            "e" | "east"            => Ok(Direction::East),
            "ne" | "northeast"      => Ok(Direction::Northeast),
            "u" | "up"              => Ok(Direction::Up),
            "d" | "down"            => Ok(Direction::Down),
            _                       => Err("invalid direction")
        }
    }

    pub fn get_dir_name(dir: Direction) -> String
    {
        match dir
        {
            Direction::North        => "north".to_string(),
            Direction::Northwest    => "northwest".to_string(),
            Direction::West         => "west".to_string(),
            Direction::Southwest    => "southwest".to_string(), 
            Direction::South        => "south".to_string(),
            Direction::Southeast    => "southeast".to_string(),
            Direction::East         => "east".to_string(),
            Direction::Northeast    => "northeast".to_string(),
            Direction::Up           => "up".to_string(),
            Direction::Down         => "down".to_string(),
        }
    }
}

