pub struct Player
{
    pub name: String,
    pub area: usize
}

impl Player
{
   pub fn new() -> Player
   {
        Player
        {
            name: "Player".to_string(),
            area: 0
        }
   }
}

