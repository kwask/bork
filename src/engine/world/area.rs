pub mod direction;

pub use direction::{
    Direction
};

pub struct Connection
{
    pub direction: Direction,
    pub area: usize
}

impl Connection
{
    pub fn new(direction: Direction, area: usize) -> Connection
    {
        Connection
        {
            direction: direction,
            area: area
        }
    }
}

pub struct Area
{
    pub name: String,
    pub description: String,
    pub id: usize,
    connections: Vec<Connection>,
    items: Vec<usize>
}

impl Area
{
    pub fn new(name: String, description: String, connections: Vec<Connection>, items: Vec<usize>, id:usize) -> Area
    {
        Area
        {
            name: name,
            description: description,
            connections: connections,
            items: items,
            id: id
        }
    }

    // get the ID of the connected room
    pub fn get_connected(&self, direction: Direction) -> Result<usize, ()>
    {
        for connection in self.connections.iter()
        {
            if direction == connection.direction
            {
                return Ok(connection.area);
            }
        }

        return Err(())
    }
}

