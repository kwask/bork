mod token;

use super::world;
use token::Token;

#[derive(Debug)]
pub enum InputError
{
    InvalidCommand,
    EmptyInput,
    ExitProgram
}

pub fn move_player(direction: &String, mut world: &world::World) -> Result<(), ()>
{
    match world.get_area(world.player.area)
    {
        Ok(area)    => Ok(()),
        Err(())     => Err(())
    }
}

pub fn process_input(input: &str, mut world: &world::World) -> Result<(), InputError>
{
    match token::tokenize_input(input)
    {
        Ok(token) =>
        {
            match token
            {
                Token::Move{direction: direction} => 
                {
                    match move_player(&direction, world)
                    {
                        Ok(())  =>
                        {
                            println!("moving {}", direction);
                            return Ok(());
                        },
                        Err(()) => println!("can't move {}", direction)
                    }
                },
                Token::Look{target: target} => 
                {
                    println!("looking at {}", target);
                    return Ok(());
                },
                Token::Exit =>
                {
                    println!("exiting");
                    return Err(InputError::ExitProgram);
                }
            }
        },
        Err(error) =>
        {
            match error
            {
                token::TokenError::InvalidCommand => 
                {
                    println!("unknown command");
                    return Err(InputError::InvalidCommand);
                },
                token::TokenError::EmptyInput => 
                    return Err(InputError::EmptyInput)
            }
        }
    }

    return Ok(());
}

