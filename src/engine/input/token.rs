#[derive(PartialEq, Eq)]
pub enum Token
{
    Move
    {
        direction: String
    },
    Look
    {
        target: String
    },
    Exit
}

#[derive(Debug)]
pub enum TokenError
{
    InvalidCommand,
    EmptyInput
}

pub fn tokenize_input(input: &str) -> Result<Token, TokenError>
{
    if input.len() <= 0
    {
        return Err(TokenError::EmptyInput);
    }

    let lowercase = input.to_lowercase();
    let words: Vec<&str> = lowercase.split(' ').collect();

    match words[0].as_ref()
    {
        "move"  => match words.get(1)
        {
            Some(direction) => Ok(Token::Move{direction: direction.to_string()}), 
            None            => Err(TokenError::InvalidCommand) 
        }
        "look"  =>
        {
            if words.len() > 1
            {
                return Ok(
                    Token::Look{target: words.get(1).unwrap().to_string()}
                );
            }

            return Ok(Token::Look{target: "area".to_string()});
        }
        "exit"  => return Ok(Token::Exit),
        _       => return Err(TokenError::InvalidCommand)
    }
}

