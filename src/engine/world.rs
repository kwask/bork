pub mod item;
pub mod area;
pub mod player;

pub struct World
{
    pub name: String,
    pub items: Vec<item::Item>,
    pub areas: Vec<area::Area>,
    pub player: player::Player
}

impl World
{
    pub fn new() -> World
    {
        World
        {
            name: "Bork".to_string(),
            items: Vec::new(),
            areas: Vec::new(),
            player: player::Player::new()
        }
    }
    
    pub fn get_area(&self, id: usize) -> Result<&area::Area, ()>
    {
        match self.areas.get(id)
        {
            Some(area)  => Ok(area),
            None        => Err(())
        }
    }
}

