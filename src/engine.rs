mod world;
mod input;

use std::io::Write;

#[derive(PartialEq, Copy, Clone)]
enum State
{
    Menu,
    Editor,
    Game,
    Exit
}

pub struct Engine
{
    state: State,
    world: world::World
}

impl Engine
{
    pub fn new() -> Engine
    {
        Engine
        {
            state: State::Menu,
            world: world::World::new()
        }
    }

    fn state(&self) -> State
    {
        return self.state;
    }

    pub fn start(&mut self)
    {
        self.state = State::Menu;
        println!("Welcome to {}, created by Nick Hirzel\nUse lowercase for \
                 all commands", self.world.name); 
    }

    pub fn process(&mut self) -> std::io::Result<()>
    {
        match self.state()
        {
            State::Menu => self.process_menu()?,
            State::Game => self.process_game()?,
            State::Editor => self.process_editor()?,
            State::Exit => return Ok(())
        }

        return Ok(())
    }

    pub fn process_menu(&mut self) -> std::io::Result<()>
    {
        println!("Main Menu\n(1) Start Game\n(2) Editor\n(3) Exit");
        
        let mut line = String::new();
        std::io::stdin().read_line(&mut line)?;
        match line.trim()
        {
            "1" => self.state = State::Game,
            "2" => self.state = State::Editor,
            "3" => self.state = State::Exit,
            _ => println!("Invalid selection, please try again")
        }

        return Ok(())
    }

    pub fn process_game(&mut self) -> std::io::Result<()>
    {
        let mut line = String::new();
        print!("What would you like to do? ");
        
        // stdout is line-buffered, necessary to manually flush
        std::io::stdout().flush().unwrap();

        let mut command_entered = false;
        while !command_entered
        {
            // getting user input
            std::io::stdin().read_line(&mut line)?;
            let input = line.trim();

            match input::process_input(input, &self.world)
            {
                Err(error)  => match error
                {
                    input::InputError::EmptyInput  => command_entered = false,
                    input::InputError::ExitProgram =>
                    {
                        self.quit();
                        return Ok(());
                    }
                    _                              => command_entered = true
                },
                Ok(()) => command_entered = true
            }
        }

        std::thread::sleep(std::time::Duration::new(0, 17));
        return Ok(())
    }    

    pub fn process_editor(&mut self) -> std::io::Result<()>
    {
        return Ok(())
    }

    pub fn quit(&mut self)
    {    
        self.state = State::Exit; 
    }

    pub fn exiting(&self) -> bool
    {
        match self.state()
        {
            State::Exit => true,
            _           => false
        }
    }
}

