mod engine;

fn main()
{
    let mut engine: engine::Engine = engine::Engine::new();
    
    engine.start();
    
    while !engine.exiting()
    {
        engine.process();
    }
}

